# (OPTIONAL: Make the terminal look more beautiful:)--------
cd && echo "export PS1='\[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h:\[\033[1;35m\]\w\[\033[1;31m\]\$\[\033[0m\] '" >> ~/.bashrc && source .bashrc
# (/OPTIONAL)


echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
sudo add-apt-repository ppa:webupd8team/atom -y
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install -y curl git openssl postgresql libpq-dev sqlite redis-server
sudo apt-get install -y vim atom
cd
git clone git://github.com/sstephenson/rbenv.git .rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL
git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
exec $SHELL
source ./.bashrc
sudo apt-get install -y libreadline-dev
rbenv install -v 2.4.3
rbenv global 2.4.3 && rbenv shell 2.4.3
ruby -v
echo "gem: --no-document" > ~/.gemrc
gem update --system
gem install bundler
gem install rails
gem install pg
rbenv rehash
git clone https://github.com/rbenv/rbenv-vars.git $(rbenv root)/plugins/rbenv-vars
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo rm /var/cache/apt/archives/lock && sudo apt-get update
sudo apt-get install -y nodejs && sudo apt-get install -y build-essential
nodejs -v
wget -qO- https://cli-assets.heroku.com/install-ubuntu.sh | sh
sudo apt-get autoremove -y
sudo update-rc.d postgresql enable
sudo update-rc.d redis-server enable
